/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package inpututilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Bruno Severo Bataglin
 */
public class InputUtilities {

    // Create an instance for Scanner class
    Scanner myKB = new Scanner(System.in);

    /**
     * Hold the application until the user press enter key
     */
    public void waitForInput() {
        try {
            System.out.println("Press ENTER to continue");
            myKB.nextLine();
        } catch (Exception e) {
            System.out.println("Something went wrong... " + e);
        }
    }

    /**
     * Ask the user to enter some text and return that text if the user does not
     * enter text, display an error and ask again
     *
     * @param prompt the question or prompt to be displayed
     * @return valid text that the user input
     */
    public String askUserForText(String prompt) {
        String userInput = "";

        myKB.nextLine(); // Consume any previous input

        while (!userInput.matches("[a-zA-Z ]+")) {
            try {
                System.out.println(prompt);
                userInput = myKB.nextLine();
            } catch (Exception e) {
                System.out.println("Please enter TEXT ONLY");
                myKB.nextLine(); // to avoid be stuck in the loop
            }
        }
        return userInput;
    }

    /**
     * Ask the user to enter some text in date format and return the text if the
     * validation is correct with the following format YYYY-MM-DD
     *
     * @param prompt
     * @return
     */
    public String askUserForDate(String prompt) {
        String userInput = "";
        boolean isValidDate = false;

        myKB.nextLine(); // Consule any previous input

        while (!isValidDate) {
            try {
                System.out.println(prompt);
                userInput = myKB.nextLine();

                DateFormat df = new SimpleDateFormat("yyyy-mm-dd");
                String after = df.format(df.parse(userInput));
                isValidDate = after.equals(userInput);

            } catch (Exception e) {
                System.out.println("The date must be YYYY-MM-DD.");
                myKB.nextLine();
            }
        }
        return userInput;
    }

    /**
     * Ask the user to enter a single word and return it if the user does not
     * enter text, display an error and ask again
     *
     * @param prompt the question or prompt to be displayed
     * @return valid text that the user input
     */
    public String askUserForSingleWord(String prompt) {
        String userInput = "";

        myKB.nextLine(); // Consume any previous input

        do {
            try {
                System.out.println(prompt);
                userInput = myKB.nextLine();
            } catch (Exception e) {
                System.out.println("Invalid input.\nPlease enter a single word without spaces or numbers");
                myKB.nextLine(); // to avoid be stuck in the loop
            }

        } while (!userInput.matches("[a-zA-Z]+") && !userInput.contains(" ") && userInput.trim().length() > 0);

        return userInput;
    }

    /**
     * Ask the user to enter an integer value if it is not an integer - display
     * an error and ask again
     *
     * @param prompt the prompt or question to be displayed
     * @return a valid integer
     */
    public int askUserForInt(String prompt) {
        int input = 0;
        boolean isValidInput = false;

        do {
            System.out.print(prompt);

            // Check if the next input is an integer
            try {
                input = myKB.nextInt();
                isValidInput = true; // Set the flag to true since we got an integer
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. Please enter an integer.");
                myKB.next(); // Consume the invalid input
            }
        } while (!isValidInput);

        return input; // Return the input value
    }

    /**
     * Ask the user to enter an integer with a given minimum allowed if it is
     * not valid then display an error and ask again
     *
     * @param prompt the question or prompt to be displayed
     * @param minimum the minimum value allowed
     * @return a valid int greater than minimum
     */
    public int askUserForInt(String prompt, int minimum) {
        int userInput = 0;
        boolean isValidInput = false;

        while (!isValidInput) {
            System.out.print(prompt);

            // Check if the next input is an integer
            try {
                userInput = myKB.nextInt();

                if (userInput >= minimum) {
                    isValidInput = true;
                } // Set the flag to true since we got an integer
                else {
                    System.out.println("Please enter an integer bigger than " + minimum + ".");
                }
            } catch (Exception e) {
                System.out.println("Invalid input. Please enter an integer number.");
                myKB.next(); // Consume the invalid input
            }
        }

        return userInput; // Return the input value
    }

    /**
     * Ask the user to enter an integer with a given minimum allowed if it is
     * not valid then display an error and ask again
     *
     * @param prompt the question or prompt to be displayed
     * @param minimum the minimum value allowed
     * @param maximum the maximum value allowed
     * @return a valid int greater than minimum
     */
    public int askUserForInt(String prompt, int minimum, int maximum) {
        int input = 0;
        boolean isValidInput = false;
        
        while (!isValidInput) {
            System.out.print(prompt);

            // Check if the next input is an integer
            try {
                input = myKB.nextInt();

                if (input >= minimum && input <= maximum) {
                    isValidInput = true; // Set the flag to true since we got an integer                    
                } else {
                    System.out.println("Please enter an integer between " + minimum + " and " + maximum + ".");
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. Please enter an integer number.");
                myKB.next(); // Consume the invalid input
            }
        }

        return input; // Return the input value
    }

    /**
     * Method to validate the user input within the options range
     *
     * @param maxOption quantity of the menu options
     * @return a valid int from options menu
     */
    public int getValidOptionInput(int maxOption) {
        int option = 0; // The default option
        boolean isValidOption = false;

        while (!isValidOption) {
            try {
                System.out.print(String.format("\nChoose an option (1-%d): ", maxOption));
                option = myKB.nextInt();

                if (option >= 1 && option <= maxOption) {
                    isValidOption = true;
                } else {
                    System.out.println(String.format("Invalid option. Please choose a number between 1 and %d.", maxOption));
                }
            } // InputMismatchException in case does not match the pattern for the expected type
            catch (InputMismatchException e) {
                System.out.println("Invalid input. Please enter a valid integer.");
                myKB.next(); // Consume the invalid input
            }
        }

        return option;
    }
}
