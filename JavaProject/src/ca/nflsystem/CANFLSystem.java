/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package ca.nflsystem;

import inpututilities.InputUtilities;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bruno Severo Bataglin
 */
public class CANFLSystem {

    /**
     * Array with the options text for the user to choose
     */
    static String[] options = 
            {
                "List all players and coach from a team",
                "List all players from a team which had an injury",
                "Search for the injuries record from a player",
                "Display how many playes from the selected skill level are in each team",
                "Display the team Win-Tie-Loss ratio",
                "Display the games which the Host win",
                "Display the Win-Tie-Loss ratio of all teams",
                "Quit the application",                
            };
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       MenuSelection();
    }
    
    /**
     * Method to display and control the user inputs on the menu at the terminal
     */
    private static void MenuSelection()
    {
         // Create an instance for Input Utilities class
        InputUtilities util = new InputUtilities();
        // Create an instance for Input Utilities class
        QueriesOptions queryOptions = new QueriesOptions();

        // While loop condition to exit
        boolean keepDoing = true;
        // A constant variable to quantity of queries we have in the app        
        final int queriesQuantity = 7;
        
        try {
            // System greetings message
            System.out.println("Welcome to NFL APP.");
            
            // Perform operations based on the user options
            while (keepDoing) {
                displayOptions();
                
                // Get the user input, validate using the input utilities class
                int option = util.getValidOptionInput(queriesQuantity + 1); // Queries + 1 (exit)

                switch (option) {
                    case 1:
                        queryOptions.GetTeamPlayers(util.askUserForText("Please, type team you would like to search:"));
                        break;
                    case 2:                        
                        queryOptions.GetTeamInjuries(util.askUserForText("Please, type team you would like to search:"));
                        break;
                    case 3:
                        queryOptions.GetPlayerInjuries(util.askUserForSingleWord("Please, type the player name you would like to search"));
                        break;
                    case 4:
                        // The first query is to display the options to the user, so they can choose from instead of having to know what are the skill levels
                        queryOptions.GetSkillLevel();
                        queryOptions.GetHowManyPlayersBySkillLevelOnTeam(util.askUserForInt("Please, type the ID of the skill level you would like to search:",1,4));
                        break;
                    case 5:
                        queryOptions.GetTeamStatistics(util.askUserForText("Please, type team you would like to search:"));
                        break;
                    case 6:
                        queryOptions.GetHostWinners(util.askUserForDate("Please insert the date of the game. Must be YYYY-MM-DD"));
                        break;
                    case 7:
                        queryOptions.GetTeamsStatistics();
                        break;
                    case queriesQuantity + 1: // Exit option
                        keepDoing = false;
                        break;
                }

                // Ask for the user to press enter to proceed only if it's not quiting the application
                if (keepDoing) {
                    util.waitForInput();
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CANFLSystem.class.getName()).log(Level.SEVERE, null, ex);
        }

        quitApplication();
    }

    /**
     * Display query option in the terminal
     */
    private static void displayOptions() {
        System.out.println("\nSelect one of the following options:");
        // Loop trhough all our options and display to the user choose
        for (int i = 0; i < options.length; i++) {
            System.out.println(String.format("%d - %s", i + 1, options[i]));
        }
    }
    
    /**
     * Method to safely quit the application
     */
    private static void quitApplication() {
        System.out.println("Exiting the program.\nGoodbye!");
        System.exit(0); // Close the program
    }
}
