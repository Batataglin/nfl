/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ca.nflsystem;

import java.sql.ResultSet; // Import the only package that we need on this class from the java SQL
import java.sql.SQLException;

/**
 *
 * @author Bruno Severo Bataglin
 */
public class QueriesOptions {
    // Create a instace to DatabaseConnection class

    DatabaseConnection dbConnection = new DatabaseConnection();

    /**
     * this is a format string, and it is used to create a spacing between the columns 
     * the '%' is the character used by the method string.Format to link with the variable to be concatenated to the string
     * after the '%' we have the columns spacing value
     * 's' is the type of the variable we are using at the format, 's' meaning we are using a string
     */
    final String tableSpacingFormat = "%-27s";

    /**
     * Display the team coach, the team players, their position, and their skill
     * level
     *
     * @param team The team to be filtered.
     * @throws SQLException
     */
    public void GetTeamPlayers(String team) throws SQLException {
        // Since the query must be with the exact name we have a if statement to prevent to duplicate error message.
        // The first query try to search for the coach from the selected team.
        // If successfully, it will proceed to the next query, display all the players from the selected team.
        if (executeQuery("SELECT CONCAT(c.FirstName, ' ', c.LastName) AS Coach "
                + "FROM coach AS c "
                + "INNER JOIN team AS t "
                + "ON c.CoachID = t.CoachID "
                + "WHERE T.Name = \"" + team + "\" ;",
                new String[]{"Coach"})) {
            // Select the players, their position, and skill level from the team and order asc by name 
            executeQuery("SELECT CONCAT(p.FirstName, ' ', p.LastName) AS Name, Pos.PositionName AS Position, Skill.SkillLevelName AS \"Skill Level\" "
                    + "FROM Player AS P "
                    + "JOIN playerPosition AS Pos ON P.PositionID = Pos.PositionID "
                    + "JOIN SkillLevel AS Skill ON P.SkillLevelID = Skill.SkillLevelID "
                    + "JOIN Team AS T ON P.TeamID = T.TeamID "
                    + "WHERE T.Name = \"" + team + "\" "
                    + "ORDER BY Name ASC;",
                    new String[]{"Name", "Position", "Skill Level"});
        }
    }

    /**
     * Display the team statistics. Win-Tie-Loss ratio
     *
     * @param team The team to be filtered
     * @throws SQLException
     */
    public void GetTeamStatistics(String team) throws SQLException {
        // This query does all its results based on the winner team.
        // It checks if the WinnerTeamID is the same of the selected team and count as a win, then sum all the records
        // if the WinnerTeamID is null on the DB, it means that the game was tie, sum the results as Tie
        // for last, if the WinnerTeamID is not null and different from the selected team, it counts as a loss
        // The query also counts all matches the team played as total games.
        // It also display the ratio of each win,tie,loss

        executeQuery("SELECT t.Name AS Team, count(*) as \"Total Games\",\n" // Count all results (win-tie-loss)
                + "SUM(IF ( WinnerTeamID = t.TeamID, 1,0)) AS Win,\n" // Count how many wins
                + "SUM(ISNULL(WinnerTeamID)) AS Tie,\n" // Count how many ties
                + "SUM(IF( WinnerTeamID != t.TeamID AND !ISNULL(WinnerTeamID), 1, 0)) AS Loss,\n" // Count how many losses
                + "concat(round(( SUM(IF ( WinnerTeamID = t.TeamID, 1,0))/count(*) * 100 ),2),'%') AS \"Win Ratio\",\n" // Display the win ratio
                + "concat(round(( SUM(ISNULL(WinnerTeamID))/count(*) * 100 ),2),'%') AS \"Tie Ratio\",\n" // Display the tie ratio
                + "concat(round(( SUM(IF( WinnerTeamID != t.TeamID AND !ISNULL(WinnerTeamID), 1, 0))/count(*) * 100 ),2),'%') AS \"Loss Ratio\"\n" // Display the loss ratio
                + "FROM game AS g\n"
                + "INNER JOIN team AS t\n"
                + "ON g.GuestTeamID = t.TeamID OR g.HostTeamID = t.TeamID\n"
                + "WHERE T.Name LIKE \"" + team + "%\""
                + "GROUP BY T.Name;",
                new String[]{"Team", "Total Games", "Win", "Tie", "Loss", "Win Ratio", "Tie Ratio", "Loss Ratio"});
    }

    /**
     * Display all the players that had an injury on the selected team
     *
     * @param team SQL where clause. Must be precise to get the results.
     * @throws SQLException
     */
    public void GetTeamInjuries(String team) throws SQLException {
        // Select all players from the seleced team and display their full name, the injury date, and the injury
        executeQuery("SELECT CONCAT(p.FirstName, ' ', p.LastName) AS Name, ir.Date, i.InjuryName AS Injury FROM injuryrecord as ir\n"
                + "inner join injury as i\n"
                + "ON ir.InjuryID = i.InjuryID\n"
                + "inner join player as p\n"
                + "ON ir.PlayerID = p.PlayerID\n"
                + "inner join team as t\n"
                + "ON p.TeamID = t.TeamID "
                + "WHERE T.Name = \"" + team + "\";",
                new String[]{"Name", "Date", "Injury"});
    }

    /**
     * Display all injuries from a selected player ordered by ascending player
     * name
     *
     * @param playerName SQL where clause to find the player names
     * @throws SQLException
     */
    public void GetPlayerInjuries(String playerName) throws SQLException {
        //  Finds any values that start with the method parameter and display their full name, position, skill level, the injuries they had, and the injury date
        // ordering by ascending player names.
        executeQuery("SELECT CONCAT(p.FirstName, ' ', p.LastName) AS \"Name\", Pos.PositionName AS \"Position\", t.Name as Team,\n"
                + "i.InjuryName AS \"Injury\", IR.Date AS \"Injury date\"\n"
                + "FROM Player AS P\n"
                + "JOIN Team as t On t.TeamID = p.TeamID\n"
                + "JOIN playerPosition AS Pos ON P.PositionID = Pos.PositionID\n"
                + "LEFT JOIN InjuryRecord AS IR ON P.PlayerID = IR.PlayerID\n"
                + "INNER JOIN injury as i ON ir.InjuryID = i.InjuryID\n"
                + "WHERE P.FirstName like \"" + playerName + "%\"\n"
                + "ORDER BY t.Name ASC;",
                new String[]{"Name", "Position", "Team", "Injury date", "Injury"});
    }

    /**
     * Display all team in the league and count how many players does the team
     * have with the selected skill level
     *
     * @param skillLevelID SQL where clause. ID of the skill level.
     * @throws SQLException
     */
    public void GetHowManyPlayersBySkillLevelOnTeam(int skillLevelID) throws SQLException {
        // Select the team names and count all players from each team have the selected skill level and group them by team
        executeQuery("SELECT t.Name AS Team, COUNT(p.PlayerID) as Players FROM player as p\n"
                + "INNER JOIN team as t\n"
                + "ON p.TeamID = t.TeamID\n"
                + "INNER JOIN skilllevel as s\n"
                + "ON p.SkillLevelID = s.SkillLevelID\n"
                + "WHERE s.SkillLevelID = " + skillLevelID + "\n"
                + "group by p.TeamID;",
                new String[]{"Team", "Players"});
    }

    /**
     * Method to show the teams win-tie-loss ordered by ascending name
     *
     * @throws SQLException
     */
    public void GetTeamsStatistics() throws SQLException {
        // This query does all its results based on the winner team.
        // It checks if the WinnerTeamID is the same of the selected team and count as a win, then sum all the records
        // if the WinnerTeamID is null on the DB, it means that the game was tie, sum the results as Tie
        // for last, if the WinnerTeamID is not null and different from the selected team, it counts as a loss
        // The query also counts all matches the team played as total games.
        // It also display the ratio of each win,tie,loss
        executeQuery("SELECT t.Name AS Team, count(*) as \"Total Games\",\n" // Count all results (win-tie-loss)
                + "SUM(IF ( WinnerTeamID = t.TeamID, 1,0)) AS Win,\n" // Count how many wins
                + "SUM(ISNULL(WinnerTeamID)) AS Tie,\n" // Count how many ties
                + "SUM(IF( WinnerTeamID != t.TeamID AND !ISNULL(WinnerTeamID), 1, 0)) AS Loss,\n" // Count how many losses
                + "concat(round(( SUM(IF ( WinnerTeamID = t.TeamID, 1,0))/count(*) * 100 ),2),'%') AS \"Win Ratio\",\n" // Display the win ratio
                + "concat(round(( SUM(ISNULL(WinnerTeamID))/count(*) * 100 ),2),'%') AS \"Tie Ratio\",\n" // Display the tie ratio
                + "concat(round(( SUM(IF( WinnerTeamID != t.TeamID AND !ISNULL(WinnerTeamID), 1, 0))/count(*) * 100 ),2),'%') AS \"Loss Ratio\"\n" // Display the loss ratio
                + "FROM game AS g\n"
                + "INNER JOIN team AS t\n"
                + "ON g.GuestTeamID = t.TeamID OR g.HostTeamID = t.TeamID\n"
                + "group by t.Name\n"
                + "order by t.Name asc;",
                new String[]{"Team", "Total Games", "Win", "Tie", "Loss", "Win Ratio", "Tie Ratio", "Loss Ratio"});
    }

    /**
     * Select the games where the winner were the host
     *
     * @param date The date which the match happened YYYY-MM-DD
     * @throws java.sql.SQLException
     */
    public void GetHostWinners(String date) throws SQLException {
        executeQuery("Select t.name as Winner, l.name as Loser, g.Score  from game as g\n"
                + "join team as t on t.teamID = g.HostTeamID\n"
                + "join team as l on l.teamID = g.GuestTeamID\n"
                + "where WinnerTeamID IN (SELECT IF(HostTeamID = WinnerTeamID, HostTeamID, GuestTeamID)) AND g.date = \"" + date + "\";",
                new String[]{"Winner", "Loser", "Score"});
    }

    /**
     * Get the skill level to display the options so the user can select from
     *
     * @throws SQLException
     */
    public void GetSkillLevel() throws SQLException {
        // Select the ID and name from each skill level in the DB
        executeQuery("SELECT skilllevelID as ID, SkillLevelName as \"Skill level\" FROM skilllevel;",
                new String[]{"ID", "Skill level"});
    }

    /**
     * Send the query to the database and print its results.
     *
     * @param query String with SQL query.
     * @param columnNames String array with the query columns
     * @return return false if query has no data.
     */
    private boolean executeQuery(String query, String[] columnNames) throws SQLException {
        try {
            // Use the method on databaseConnection class to execute the query
            // Store the query result to use localy
            ResultSet queryResults = dbConnection.executeQuery(query);

            // Check if the result set is not null
            if (queryResults.isBeforeFirst()) {
                printTableHeader(columnNames);

                // While loop through the ResultSet, using the local method to print into the console
                while (queryResults.next()) {
                    printRow(queryResults, columnNames);
                }

                // Add a space line at the end of the list
                //System.out.println("\n");
            } else {
                System.out.println("The query has no result.\n");
                return false;
            }
        } catch (SQLException e) {
            System.out.println("ERROR " + e);
        } finally {
            // Close the connection to the db
            dbConnection.closeConnection();
        }
        return true;
    }

    /**
     * Create the header table with the column names
     *
     * @param columnNames string array with column name
     */
    private void printTableHeader(String[] columnNames) {
        // StringBuilder is used to do a better manipulation of strings, avoiding usage of '+' to concatenate
        StringBuilder header = new StringBuilder("\n");

        // Loop for each element of the array with the column names
        for (String columnName : columnNames) {
            // use the method append to concatenate the formatted text into our header variable
            header.append(String.format(tableSpacingFormat, columnName));
        }

        // Print the header into the terminal
        System.out.println(header.toString());
    }

    /**
     * Print the result set row into the terminal
     *
     * @param resultSet Set of result of the query
     * @param columnNames array with the column to search into the result set
     * @throws SQLException
     */
    private void printRow(ResultSet resultSet, String[] columnNames) throws SQLException {
        // StringBuilder is used to do a better manipulation of string, avoiding usage of + to concatenate
        StringBuilder row = new StringBuilder();

        // Loop for each element of the array with the column names
        for (String columnName : columnNames) {
            // use the method append to concatenate the formatted text into our row variable
            // The format will get the data on the result set based on the column name.
            row.append(String.format(tableSpacingFormat, resultSet.getString(columnName)));
        }

        // Print the row into the terminal
        System.out.println(row.toString());
    }

}
