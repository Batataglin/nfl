/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ca.nflsystem;

import java.sql.*;

/**
 *
 * @author Bruno Severo Bataglin
 */
public class DatabaseConnection {

    // Constant variables which cannot be changed at runtime
    final String databaseName = "nflleague";
    final String databaseUser = "root";
    final String databasePassword = "pass1234!";

    private static Connection con;
    private static boolean isConnected = false;

    /**
     * Try to connect to the database
     */
    private void tryToConnect() {
        try {
            // If its already connected, return to not connect again.
            if (isConnected) {
                return;
            }

            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(String.format("jdbc:mysql://localhost:3306/%s", databaseName),
                    databaseUser, databasePassword);
          
        } catch (SQLException e) {
            System.out.println("SQL Error --> ");
            System.out.println(e.getMessage());  // prints out the java error
            System.out.println(e.getSQLState()); // print out the exact message from the DB error
            isConnected = false;
        } catch (ClassNotFoundException e) {
            System.out.println("Class error -- " + e.getMessage());
            isConnected = false;
        } finally {
              isConnected = true;
        }
    }

    /**
     * Close the connection with the database
     *
     * @throws SQLException
     */
    public void closeConnection() throws SQLException {
        try {
            // Close the connection with the data base
            con.close();
            
            // Set false since the database is not connected anymore
            isConnected = false;
        } catch (SQLException ex) {
            System.out.println("Error on closing the DB connection. " + ex);
        }
    }

    /**
     * Execute the SQL query and return the results
     *
     * @param query SQL query
     * @return the result of the query
     * @throws java.sql.SQLException
     */
    public ResultSet executeQuery(String query) {
        ResultSet queryResults = null;

        try {
            // Try to connect to the database to execute the query
            tryToConnect();

            // Create a statement and execure the query and alocate the query result into a local variable
            queryResults = con.createStatement().executeQuery(query);
        } catch (SQLException e) {
            System.out.print("An error have occured executing the query.\n" + e);

        }
        return queryResults;
    }
}
