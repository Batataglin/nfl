CREATE DATABASE nflleague;
USE  nflleague;

-- Create PlayerPosition table
CREATE TABLE PlayerPosition (
    PositionID INT PRIMARY KEY AUTO_INCREMENT ,
    PositionName VARCHAR(40) NOT NULL
);

-- Create SkillLevel table
CREATE TABLE SkillLevel (
    SkillLevelID INT PRIMARY KEY AUTO_INCREMENT,
    SkillLevelName VARCHAR(40) NOT NULL
);

-- Create Coach table
CREATE TABLE Coach (
    CoachID INT PRIMARY KEY AUTO_INCREMENT,
    FirstName VARCHAR(40) NOT NULL,
    LastName VARCHAR(60) NOT NULL
);

-- NEED TO CREATE TABLE THEN ALTER TO ADD FK
-- Create Team table
CREATE TABLE Team (
    TeamID INT PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(40) NOT NULL,
    City VARCHAR(40) NOT NULL,
    CoachID INT,
    FOREIGN KEY (CoachID) REFERENCES Coach(CoachID)
);
 
/* Create Boolean variable for captain to avoid corelation between team and player */
-- Create Player table
CREATE TABLE Player (
    PlayerID INT PRIMARY KEY AUTO_INCREMENT,
    FirstName VARCHAR(40) NOT NULL,
    LastName VARCHAR(60) NOT NULL,
    PositionID INT NOT NULL,
    SkillLevelID INT NOT NULL,
    TeamID INT NOT NULL,
    Captain bool,
    FOREIGN KEY (PositionID) REFERENCES PlayerPosition(PositionID),
    FOREIGN KEY (SkillLevelID) REFERENCES SkillLevel(SkillLevelID),
    FOREIGN KEY (TeamID) REFERENCES Team(TeamID)
);

-- Create Injury table
CREATE TABLE Injury (
    InjuryID INT PRIMARY KEY AUTO_INCREMENT,
	InjuryName VARCHAR(255) NOT NULL
);

-- Create InjuryRecord table
CREATE TABLE InjuryRecord (
    InjuryRecordID INT PRIMARY KEY AUTO_INCREMENT,
    Date DATE NOT NULL, 
    PlayerID INT NOT NULL,
    InjuryID INT NOT NULL,
    FOREIGN KEY (PlayerID) REFERENCES Player(PlayerID),
    FOREIGN KEY (InjuryID) REFERENCES Injury(InjuryID)
);

-- Create Game table
CREATE TABLE Game (
    GameID INT PRIMARY KEY AUTO_INCREMENT,
    HostTeamID INT NOT NULL,
    GuestTeamID INT NOT NULL,
    WinnerTeamID INT,
	Score VARCHAR(10) NOT NULL,
    Date DATE NOT NULL,
    FOREIGN KEY (HostTeamID) REFERENCES Team(TeamID),
    FOREIGN KEY (GuestTeamID) REFERENCES Team(TeamID),
    FOREIGN KEY (WinnerTeamID) REFERENCES Team(TeamID)
);

-- excercise 5--
-- You must have a minimum of 10 teams in the League. --

/* INSERT INTO Coach */
INSERT INTO Coach (FirstName, LastName) 
VALUES('John','Harbaugh'),
('Sean','McDermott'),
('Zac','Taylor'),
('Kevin','Stefanski'),
('Sean','Payton'),
('DeMeco','Ryans'),
('Shane','Steichen'),
('Doug','Pederson'),
('Andy','Reid'),
('Antonio','Pierce'),
('Brandon','Staley');

/* INSERT INTO Team */
INSERT INTO Team (Name, City, CoachID) 
VALUES('Baltimore Ravens','Baltimore', 1),
('Buffalo Bills','Buffalo', 2),
('Cincinnati Bengals','Cincinnati', 3),
('Cleveland Browns','Cleveland', 4),
('Denver Broncos','Denver', 5),
('Houston Texans','Houston', 6),
('Indianapolis Colts','Indianapolis', 7),
('Jacksonville Jaguars','Jacksonville', 8),
('Kansas City Chiefs','Kansas City', 9),
('Las Vegas Raiders','Las Vegas', 10),
('Los Angeles Chargers','Los Angeles', 11);

/* INSERT INTO PlayerPosition */
INSERT INTO PlayerPosition (PositionName) VALUES ('Quarterback'),
('Running Back'),
('Wide Receiver'),
('Tight End'),
('Offensive Tackle'),
('Offensive Guard'),
('Center'),
('Defensive Tackle'),
('Defensive End'),
('Linebacker'),
('Cornerback'),
('Safety'),
('Nickelback'),
('Dimeback'),
('Kicker'),
('Punter'),
('Long Snapper'),
('Fullback'),
('H-Back');

/* Insert Skill Levels */
INSERT INTO SkillLevel (SkillLevelName) 
VALUES ('Pro'),
('Star'),
('Elite'),
('Rising Star');

/* INSERT INTO Player */
INSERT INTO Player (FirstName, LastName, PositionID, SkillLevelID, TeamID, Captain)
VALUES ('Dewayne','Patrick',1,1,1,1),
('Henry','Le',2,1,1,0),
('Kurt','Burnett',3,1,1,0),
('Brooks','Mahoney',4,4,1,0),
('Johnson','Hays',5,4,1,0),
('Earnest','Thomas',6,4,1,0),
('Thanh','Hunt',7,2,1,0),
('Dillon','Johnston',8,3,1,0),
('Alex','Schultz',9,3,1,0),
('Lyman','Kramer',10,3,1,0),
('Jc','Rios',11,4,1,0),
('Eliseo','Graham',12,1,1,0),
('Alfredo','Garner',13,4,1,0),
('Scot','Francis',14,2,1,0),
('Merle','Jefferson',15,4,1,0),
('Eric','Zamora',16,4,1,0),
('Philip','Colon',17,1,1,0),
('Pablo','Contreras',18,3,1,0),
('Douglas','Downs',19,2,1,0),
('Clarence','Powell',1,3,1,0),
('Damon','Bond',1,3,2,1),
('Edison','Conway',2,1,2,0),
('Russell','Sanchez',3,4,2,0),
('Lon','Kim',4,2,2,0),
('Chad','Macdonald',5,4,2,0),
('Jeremiah','Maynard',6,2,2,0),
('Carlo','Farmer',7,4,2,0),
('Billy','Lambert',8,3,2,0),
('Earle','Mccann',9,4,2,0),
('Garfield','Carr',10,3,2,0),
('Kevin','Wallace',11,2,2,0),
('Loyd','Barber',12,1,2,0),
('Terrence','Moore',13,1,2,0),
('Anibal','Conley',14,1,2,0),
('Donovan','Bowers',15,4,2,0),
('Zachariah','Spears',16,1,2,0),
('Donnell','Booker',17,2,2,0),
('Percy','Hardy',18,2,2,0),
('Rico','Salas',19,2,2,0),
('Damien','Ellison',15,1,2,0),
('Stephen','Atkins',1,3,3,1),
('Gene','Olson',2,3,3,0),
('Titus','Kline',3,1,3,0),
('Ethan','Nash',4,3,3,0),
('Andy','Hawkins',5,1,3,0),
('Randolph','Harris',6,3,3,0),
('Eldon','Robinson',7,2,3,0),
('Orville','Anderson',8,2,3,0),
('Nigel','Hart',9,4,3,0),
('Genaro','Nunez',10,1,3,0),
('Marquis','Fry',11,3,3,0),
('Clyde','Burnett',12,4,3,0),
('Ira','Murillo',13,2,3,0),
('Ashley','Mathews',14,3,3,0),
('Blair','Mendez',15,4,3,0),
('Rudolf','Larson',16,1,3,0),
('Merle','Greer',17,4,3,0),
('Efren','Gonzalez',18,3,3,0),
('Trenton','Horton',19,1,3,0),
('Dion','Copeland',12,4,3,0),
('Leonard','Gamble',1,4,4,1),
('Domingo','Rogers',2,2,4,0),
('Markus','Kerr',3,3,4,0),
('Timmy','Hatfield',4,2,4,0),
('Duncan','Galloway',5,2,4,0),
('Pierre','Chase',6,1,4,0),
('Winfred','Jensen',7,3,4,0),
('Virgilio','Schmidt',8,1,4,0),
('Benton','Mata',9,1,4,0),
('Bruce','Norris',10,2,4,0),
('Colin','White',11,3,4,0),
('Waldo','Zuniga',12,2,4,0),
('Horace','Logan',13,4,4,0),
('Harold','Wu',14,1,4,0),
('Barrett','Ramirez',15,1,4,0),
('Hoyt','Dudley',16,4,4,0),
('Danial','Li',17,3,4,0),
('Marcel','Barron',18,2,4,0),
('Stewart','Ellison',19,3,4,0),
('Donnie','Ponce',1,4,4,0),
('Hershel','Barton',1,1,5,1),
('Olen','Benton',2,1,5,0),
('Kasey','Odonnell',3,2,5,0),
('Tommie','Gilmore',4,2,5,0),
('Alan','Skinner',5,2,5,0),
('Shad','Shaw',6,3,5,0),
('Carey','Freeman',7,2,5,0),
('Vaughn','Anthony',8,3,5,0),
('Moises','Carlson',9,3,5,0),
('Trey','Wolfe',10,2,5,0),
('Warner','Joyce',11,1,5,0),
('Duane','Newman',12,2,5,0),
('Mark','Franklin',13,3,5,0),
('Jarred','Fleming',14,4,5,0),
('Coleman','Carney',15,3,5,0),
('Bennie','Peterson',16,2,5,0),
('Jamal','Marquez',17,4,5,0),
('Jamel','Underwood',18,2,5,0),
('Aurelio','Gross',19,4,5,0),
('Elvin','Diaz',4,4,5,0),
('Claud','Harper',1,2,6,1),
('Marcos','Mathis',2,1,6,0),
('Moses','Mcbride',3,3,6,0),
('Sydney','Ferguson',4,1,6,0),
('Kristofer','Mccoy',5,1,6,0),
('Randy','Williams',6,4,6,0),
('Laverne','Baker',7,4,6,0),
('Todd','Long',8,3,6,0),
('Joseph','Tate',9,3,6,0),
('Tuan','Dodson',10,3,6,0),
('Danial','Ballard',11,3,6,0),
('Reuben','Mason',12,2,6,0),
('Reinaldo','Jennings',13,4,6,0),
('Jeromy','Wells',14,2,6,0),
('Cruz','Whitaker',15,2,6,0),
('Dominick','Best',16,3,6,0),
('Donnell','Bonilla',17,3,6,0),
('Levi','Copeland',18,1,6,0),
('Wendell','Franklin',19,2,6,0),
('Buford','Travis',17,3,6,0),
('Alfredo','Marks',1,4,7,1),
('Michael','Holt',2,3,7,0),
('Archie','Branch',3,1,7,0),
('Lincoln','Rasmussen',4,4,7,0),
('Winfred','Rangel',5,3,7,0),
('Jackie','Vargas',6,3,7,0),
('Adalberto','Andersen',7,2,7,0),
('Hassan','Lee',8,4,7,0),
('Errol','Bradford',9,1,7,0),
('Randolph','Zavala',10,4,7,0),
('Everett','Merritt',11,1,7,0),
('Zachariah','Cook',12,2,7,0),
('Cary','Carlson',13,4,7,0),
('Jim','Krueger',14,4,7,0),
('Hyman','Sanford',15,4,7,0),
('Monroe','Werner',16,3,7,0),
('Jayson','Acevedo',17,1,7,0),
('Ramon','Chapman',18,4,7,0),
('Williams','Dunn',19,3,7,0),
('Timothy','Collins',5,1,7,0),
('Cesar','Shelton',1,1,8,1),
('Kelvin','Becker',2,1,8,0),
('Kirk','Mayo',3,3,8,0),
('William','Solomon',4,3,8,0),
('Russel','Lozano',5,1,8,0),
('Angelo','Jarvis',6,4,8,0),
('Fletcher','Campbell',7,4,8,0),
('Deandre','Mccall',8,2,8,0),
('Steve','Evans',9,4,8,0),
('Roberto','Potter',10,2,8,0),
('Richie','Wise',11,2,8,0),
('Wilmer','Reilly',12,4,8,0),
('Eldon','Jackson',13,1,8,0),
('Renato','Allison',14,2,8,0),
('Elden','Gamble',15,1,8,0),
('Craig','Yoder',16,2,8,0),
('Reynaldo','Yu',17,1,8,0),
('Pasquale','Merritt',18,2,8,0),
('Jonathan','Keller',19,1,8,0),
('Anibal','Cunningham',15,1,8,0),
('Ramon','Gardner',1,2,9,1),
('Otis','Macias',2,2,9,0),
('Morris','Shah',3,3,9,0),
('Juan','Giles',4,4,9,0),
('Benito','Schultz',5,1,9,0),
('Enoch','Mccall',6,3,9,0),
('Keneth','Pacheco',7,4,9,0),
('Ty','Holloway',8,1,9,0),
('Mohammed','Reese',9,3,9,0),
('Chauncey','Weaver',10,1,9,0),
('Roberto','Cross',11,1,9,0),
('Coleman','Branch',12,1,9,0),
('Ben','Mcpherson',13,2,9,0),
('Hosea','Patrick',14,2,9,0),
('Gary','Esparza',15,3,9,0),
('Reyes','George',16,1,9,0),
('Leroy','Guerrero',17,3,9,0),
('Hipolito','Zhang',18,4,9,0),
('Dusty','Stephenson',19,3,9,0),
('Lino','Sheppard',6,2,9,0),
('Brendon','Ali',1,1,10,1),
('Milton','Bridges',2,3,10,0),
('Booker','Ochoa',3,4,10,0),
('Darwin','Boone',4,4,10,0),
('Elmer','Whitney',5,1,10,0),
('Elisha','Hester',6,3,10,0),
('Miguel','Robbins',7,3,10,0),
('Edmundo','Holland',8,3,10,0),
('Kenneth','Hart',9,3,10,0),
('Kennith','Cain',10,4,10,0),
('Errol','Leblanc',11,2,10,0),
('Steve','Fernandez',12,2,10,0),
('Benton','Kirby',13,4,10,0),
('Cedrick','Perez',14,2,10,0),
('Dana','Moss',15,2,10,0),
('Lazaro','Atkinson',16,1,10,0),
('Gavin','Paul',17,2,10,0),
('Bill','Beard',18,2,10,0),
('Kurt','Hayes',19,4,10,0),
('Garret','Brooks',13,2,10,0),
('Heath','Harding',1,2,11,1),
('Kennith','Bruce',2,1,11,0),
('Jed','Mcgee',3,3,11,0),
('Ruben','Lane',4,1,11,0),
('Russel','Daugherty',5,4,11,0),
('Ramon','Crane',6,4,11,0),
('Wendell','Soto',7,3,11,0),
('Shawn','Lin',8,3,11,0),
('Buck','Walter',9,3,11,0),
('Lucas','Mcclain',10,1,11,0),
('Rolland','Garner',11,4,11,0),
('Felton','Watts',12,3,11,0),
('Eliseo','Becker',13,4,11,0),
('Erasmo','Lutz',14,4,11,0),
('Neville','Mercer',15,4,11,0),
('Delmar','Lee',16,4,11,0),
('Lacy','Hess',17,4,11,0),
('Rueben','Jacobs',18,2,11,0),
('Randy','Gamble',19,2,11,0),
('Santos','Berger',12,2,11,0);

/* INSERT INTO Game */
INSERT INTO Game (HostTeamID, GuestTeamID, WinnerTeamID, Score, Date)
VALUES (1,5, null,'20to20','2023-08-14'),
(11,2,null,'30to30','2023-08-14'),
(9,7,7,'27to32','2023-08-14'),
(3,8,null,'27to27','2023-08-14'),
(4,6,4,'17to29','2023-08-14'),
(5,3,3,'50to31','2023-08-19'),
(1,2,1,'35to15','2023-08-19'),
(10,8,8,'47to35','2023-08-19'),
(3,4,4,'55to24','2023-09-01'),
(8,11,null,'45to45','2023-09-01'),
(7,6,7,'25to19','2023-09-01'),
(9,2,9,'23to17','2023-09-04'),
(5,1,1,'41to35','2023-09-04'),
(10,4,null,'10to10','2023-09-04'),
(3,7,null,'20to20','2023-09-04'),
('6', '8', '6', '18to26', '2023-09-04');
 
/* INSERT INTO Injury */
INSERT INTO Injury (InjuryName)
VALUES ('Sholder dislocation'),
('Tibial fracture'),
('Right 5th finger fracture'),
('Severe fracture to the lower left leg'),
('Knee injury'),
('Patellar tendon rupture'),
('Fracture to the lower right leg'),
('Ankcle sprains'),
('Neck injury'),
('Patellar dislocatino'),
('Left 3th finger fracture');

/* INSERT INTO InjuryRecord */
INSERT INTO InjuryRecord (date,playerID, InjuryID)
VALUES ('2023-04-22', 54, 1),
('2023-04-30', 8, 2),
('2023-04-28', 76, 3),
('2023-05-02', 99, 4),
('2023-05-22', 125, 5),
('2023-05-17', 76, 6),
('2023-05-16', 54, 1),
('2023-06-23', 125, 7),
('2023-07-05', 54, 8),
('2023-07-05', 89, 9),
('2023-07-31', 200, 2),
('2023-08-14', 8, 10),
('2023-08-26', 150, 11),
('2023-09-08', 124, 1),
('2023-09-08', 8, 1),
('2023-09-08', 8, 3),
('2023-09-08', 99, 8);
 
-- ------- PART 2 ------- --
-- 1. List all captains ID with their first, last name and Team 
 SELECT PlayerID, FirstName, LastName, TeamID FROM nflleague.player WHERE Captain = 1;

-- 2. List all unique positions found in the database
SELECT Distinct * FROM nflleague.playerposition;
 
-- 3. List all the football players’ names ordered alphabetically in ascending order, i.e. first name and last 
-- name in alphabetical order, for a particular team, and display their Last Name first
SELECT LastName, FirstName FROM nflleague.player 
WHERE TeamID = 3 ORDER BY LastName ASC, FirstName  ASC;

-- 4. Count how many teams won, lost or draw on the last matchday
SELECT SUM(ISNULL(WinnerTeamID)) as DrawTeams, SUM(!ISNULL(WinnerTeamID)) as WinnerTeams, SUM(!ISNULL(WinnerTeamID)) AS LoserTeams FROM nflleague.game WHERE Date = '2023-09-04';

-- 5. Provide the list with all the details of the football players, of the teams that lost the first matchday, 
-- and sort them by skill level in ascending order
SELECT player.*, skill.SkillLevelName FROM nflleague.player AS player
INNER JOIN nflleague.skilllevel AS skill
ON player.SkillLevelID = skill.SkillLevelID
where player.TeamID IN (SELECT IF(HostTeamID = WinnerTeamID, GuestTeamID, HostTeamID)
FROM nflleague.game WHERE WinnerTeamID IS NOT NULL AND Date = '2023-08-14')
ORDER BY skill.SkillLevelName ASC;

-- 6. How many football players with a low skill level had or has an injury?
SELECT Count(DISTINCT (player.playerID)) AS PlayersWithInjury FROM nflleague.player AS player
INNER JOIN nflleague.injuryrecord AS injury
ON player.PlayerID = injury.PlayerID
WHERE player.SkillLevelID = 4;

-- 7. How many football players have had more than one injury? Display the information of the football 
-- player, including the information of the Team (name and city) and they type and the dates of the 
-- injuries.
SELECT player.*, team.Name AS TeamName, team.City as TeamCity, injury.InjuryName, injuryR.Date FROM nflleague.player AS player
INNER JOIN nflleague.injuryrecord AS injuryR
ON player.PlayerID = injuryR.PlayerID
INNER JOIN nflleague.injury AS injury
ON injuryR.InjuryID = injury.InjuryID
INNER JOIN nflleague.team AS team
ON player.TeamID = team.TeamID
WHERE player.PlayerID IN (SELECT PlayerID FROM nflleague.injuryrecord GROUP BY PlayerID HAVING COUNT(PlayerID) > 1)
ORDER BY player.FirstName; 

-- 8. Display a list of the most common injuries and order them by ascending order.
SELECT injury.InjuryName, Count(injuryR.InjuryID) AS Total 
FROM nflleague.injuryrecord AS injuryR
INNER JOIN nflleague.injury as injury
ON injuryR.InjuryID = injury.InjuryID
GROUP BY injury.InjuryName
ORDER BY Total ASC;

-- 9. Which match date has more draws? Show a list of the teams that have draw that date and their 
-- score.
SELECT hTeam.Name AS HostTeam, gTeam.Name AS GuestTeam, game.Score, game.Date FROM nflleague.game AS game 
INNER JOIN nflleague.team AS hTeam
ON game.HostTeamID = hTeam.TeamID
INNER JOIN nflleague.team AS gTeam
ON game.GuestTeamID = gTeam.TeamID
WHERE Date =
(SELECT Date FROM nflleague.game AS g WHERE WinnerTeamID IS NULL GROUP BY g.Date HAVING count(g.Date)
ORDER BY count(Date) DESC LIMIT 1) AND WinnerTeamID IS NULL;

-- 10. Count how many players have a low skill level, medium skill level and high skill level, group them by 
-- team and sort them in an increasing order.
SELECT team.Name as TeamName , skill.SkillLevelName, count(player.PlayerID) AS Players FROM nflleague.player AS player
INNER JOIN nflleague.skilllevel AS skill
ON player.SkillLevelID = skill.SkillLevelID
INNER JOIN nflleague.team AS team 
ON player.TeamID = team.TeamID
GROUP BY team.TeamID, skill.SkillLevelID
ORDER BY Players ASC;
 