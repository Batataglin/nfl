-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: nflleague
-- ------------------------------------------------------
-- Server version	8.0.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `injuryrecord`
--

DROP TABLE IF EXISTS `injuryrecord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `injuryrecord` (
  `InjuryRecordID` int NOT NULL AUTO_INCREMENT,
  `Date` date NOT NULL,
  `PlayerID` int NOT NULL,
  `InjuryID` int NOT NULL,
  PRIMARY KEY (`InjuryRecordID`),
  KEY `PlayerID` (`PlayerID`),
  KEY `InjuryID` (`InjuryID`),
  CONSTRAINT `injuryrecord_ibfk_1` FOREIGN KEY (`PlayerID`) REFERENCES `player` (`PlayerID`),
  CONSTRAINT `injuryrecord_ibfk_2` FOREIGN KEY (`InjuryID`) REFERENCES `injury` (`InjuryID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `injuryrecord`
--

LOCK TABLES `injuryrecord` WRITE;
/*!40000 ALTER TABLE `injuryrecord` DISABLE KEYS */;
INSERT INTO `injuryrecord` VALUES (1,'2023-04-22',54,1),(2,'2023-04-30',8,2),(3,'2023-04-28',76,3),(4,'2023-05-02',99,4),(5,'2023-05-22',125,5),(6,'2023-05-17',76,6),(7,'2023-05-16',54,1),(8,'2023-06-23',125,7),(9,'2023-07-05',54,8),(10,'2023-07-05',89,9),(11,'2023-07-31',200,2),(12,'2023-08-14',8,10),(13,'2023-08-26',150,11),(14,'2023-09-08',124,1),(15,'2023-09-08',8,1),(16,'2023-09-08',8,3),(17,'2023-09-08',99,8);
/*!40000 ALTER TABLE `injuryrecord` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-03-11  9:28:36
