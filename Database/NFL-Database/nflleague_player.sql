-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: nflleague
-- ------------------------------------------------------
-- Server version	8.0.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `player`
--

DROP TABLE IF EXISTS `player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `player` (
  `PlayerID` int NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(40) NOT NULL,
  `LastName` varchar(60) NOT NULL,
  `PositionID` int NOT NULL,
  `SkillLevelID` int NOT NULL,
  `TeamID` int NOT NULL,
  `Captain` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`PlayerID`),
  KEY `PositionID` (`PositionID`),
  KEY `SkillLevelID` (`SkillLevelID`),
  KEY `TeamID` (`TeamID`),
  CONSTRAINT `player_ibfk_1` FOREIGN KEY (`PositionID`) REFERENCES `playerposition` (`PositionID`),
  CONSTRAINT `player_ibfk_2` FOREIGN KEY (`SkillLevelID`) REFERENCES `skilllevel` (`SkillLevelID`),
  CONSTRAINT `player_ibfk_3` FOREIGN KEY (`TeamID`) REFERENCES `team` (`TeamID`)
) ENGINE=InnoDB AUTO_INCREMENT=221 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player`
--

LOCK TABLES `player` WRITE;
/*!40000 ALTER TABLE `player` DISABLE KEYS */;
INSERT INTO `player` VALUES (1,'Dewayne','Patrick',1,1,1,1),(2,'Henry','Le',2,1,1,0),(3,'Kurt','Burnett',3,1,1,0),(4,'Brooks','Mahoney',4,4,1,0),(5,'Johnson','Hays',5,4,1,0),(6,'Earnest','Thomas',6,4,1,0),(7,'Thanh','Hunt',7,2,1,0),(8,'Dillon','Johnston',8,3,1,0),(9,'Alex','Schultz',9,3,1,0),(10,'Lyman','Kramer',10,3,1,0),(11,'Jc','Rios',11,4,1,0),(12,'Eliseo','Graham',12,1,1,0),(13,'Alfredo','Garner',13,4,1,0),(14,'Scot','Francis',14,2,1,0),(15,'Merle','Jefferson',15,4,1,0),(16,'Eric','Zamora',16,4,1,0),(17,'Philip','Colon',17,1,1,0),(18,'Pablo','Contreras',18,3,1,0),(19,'Douglas','Downs',19,2,1,0),(20,'Clarence','Powell',1,3,1,0),(21,'Damon','Bond',1,3,2,1),(22,'Edison','Conway',2,1,2,0),(23,'Russell','Sanchez',3,4,2,0),(24,'Lon','Kim',4,2,2,0),(25,'Chad','Macdonald',5,4,2,0),(26,'Jeremiah','Maynard',6,2,2,0),(27,'Carlo','Farmer',7,4,2,0),(28,'Billy','Lambert',8,3,2,0),(29,'Earle','Mccann',9,4,2,0),(30,'Garfield','Carr',10,3,2,0),(31,'Kevin','Wallace',11,2,2,0),(32,'Loyd','Barber',12,1,2,0),(33,'Terrence','Moore',13,1,2,0),(34,'Anibal','Conley',14,1,2,0),(35,'Donovan','Bowers',15,4,2,0),(36,'Zachariah','Spears',16,1,2,0),(37,'Donnell','Booker',17,2,2,0),(38,'Percy','Hardy',18,2,2,0),(39,'Rico','Salas',19,2,2,0),(40,'Damien','Ellison',15,1,2,0),(41,'Stephen','Atkins',1,3,3,1),(42,'Gene','Olson',2,3,3,0),(43,'Titus','Kline',3,1,3,0),(44,'Ethan','Nash',4,3,3,0),(45,'Andy','Hawkins',5,1,3,0),(46,'Randolph','Harris',6,3,3,0),(47,'Eldon','Robinson',7,2,3,0),(48,'Orville','Anderson',8,2,3,0),(49,'Nigel','Hart',9,4,3,0),(50,'Genaro','Nunez',10,1,3,0),(51,'Marquis','Fry',11,3,3,0),(52,'Clyde','Burnett',12,4,3,0),(53,'Ira','Murillo',13,2,3,0),(54,'Ashley','Mathews',14,3,3,0),(55,'Blair','Mendez',15,4,3,0),(56,'Rudolf','Larson',16,1,3,0),(57,'Merle','Greer',17,4,3,0),(58,'Efren','Gonzalez',18,3,3,0),(59,'Trenton','Horton',19,1,3,0),(60,'Dion','Copeland',12,4,3,0),(61,'Leonard','Gamble',1,4,4,1),(62,'Domingo','Rogers',2,2,4,0),(63,'Markus','Kerr',3,3,4,0),(64,'Timmy','Hatfield',4,2,4,0),(65,'Duncan','Galloway',5,2,4,0),(66,'Pierre','Chase',6,1,4,0),(67,'Winfred','Jensen',7,3,4,0),(68,'Virgilio','Schmidt',8,1,4,0),(69,'Benton','Mata',9,1,4,0),(70,'Bruce','Norris',10,2,4,0),(71,'Colin','White',11,3,4,0),(72,'Waldo','Zuniga',12,2,4,0),(73,'Horace','Logan',13,4,4,0),(74,'Harold','Wu',14,1,4,0),(75,'Barrett','Ramirez',15,1,4,0),(76,'Hoyt','Dudley',16,4,4,0),(77,'Danial','Li',17,3,4,0),(78,'Marcel','Barron',18,2,4,0),(79,'Stewart','Ellison',19,3,4,0),(80,'Donnie','Ponce',1,4,4,0),(81,'Hershel','Barton',1,1,5,1),(82,'Olen','Benton',2,1,5,0),(83,'Kasey','Odonnell',3,2,5,0),(84,'Tommie','Gilmore',4,2,5,0),(85,'Alan','Skinner',5,2,5,0),(86,'Shad','Shaw',6,3,5,0),(87,'Carey','Freeman',7,2,5,0),(88,'Vaughn','Anthony',8,3,5,0),(89,'Moises','Carlson',9,3,5,0),(90,'Trey','Wolfe',10,2,5,0),(91,'Warner','Joyce',11,1,5,0),(92,'Duane','Newman',12,2,5,0),(93,'Mark','Franklin',13,3,5,0),(94,'Jarred','Fleming',14,4,5,0),(95,'Coleman','Carney',15,3,5,0),(96,'Bennie','Peterson',16,2,5,0),(97,'Jamal','Marquez',17,4,5,0),(98,'Jamel','Underwood',18,2,5,0),(99,'Aurelio','Gross',19,4,5,0),(100,'Elvin','Diaz',4,4,5,0),(101,'Claud','Harper',1,2,6,1),(102,'Marcos','Mathis',2,1,6,0),(103,'Moses','Mcbride',3,3,6,0),(104,'Sydney','Ferguson',4,1,6,0),(105,'Kristofer','Mccoy',5,1,6,0),(106,'Randy','Williams',6,4,6,0),(107,'Laverne','Baker',7,4,6,0),(108,'Todd','Long',8,3,6,0),(109,'Joseph','Tate',9,3,6,0),(110,'Tuan','Dodson',10,3,6,0),(111,'Danial','Ballard',11,3,6,0),(112,'Reuben','Mason',12,2,6,0),(113,'Reinaldo','Jennings',13,4,6,0),(114,'Jeromy','Wells',14,2,6,0),(115,'Cruz','Whitaker',15,2,6,0),(116,'Dominick','Best',16,3,6,0),(117,'Donnell','Bonilla',17,3,6,0),(118,'Levi','Copeland',18,1,6,0),(119,'Wendell','Franklin',19,2,6,0),(120,'Buford','Travis',17,3,6,0),(121,'Alfredo','Marks',1,4,7,1),(122,'Michael','Holt',2,3,7,0),(123,'Archie','Branch',3,1,7,0),(124,'Lincoln','Rasmussen',4,4,7,0),(125,'Winfred','Rangel',5,3,7,0),(126,'Jackie','Vargas',6,3,7,0),(127,'Adalberto','Andersen',7,2,7,0),(128,'Hassan','Lee',8,4,7,0),(129,'Errol','Bradford',9,1,7,0),(130,'Randolph','Zavala',10,4,7,0),(131,'Everett','Merritt',11,1,7,0),(132,'Zachariah','Cook',12,2,7,0),(133,'Cary','Carlson',13,4,7,0),(134,'Jim','Krueger',14,4,7,0),(135,'Hyman','Sanford',15,4,7,0),(136,'Monroe','Werner',16,3,7,0),(137,'Jayson','Acevedo',17,1,7,0),(138,'Ramon','Chapman',18,4,7,0),(139,'Williams','Dunn',19,3,7,0),(140,'Timothy','Collins',5,1,7,0),(141,'Cesar','Shelton',1,1,8,1),(142,'Kelvin','Becker',2,1,8,0),(143,'Kirk','Mayo',3,3,8,0),(144,'William','Solomon',4,3,8,0),(145,'Russel','Lozano',5,1,8,0),(146,'Angelo','Jarvis',6,4,8,0),(147,'Fletcher','Campbell',7,4,8,0),(148,'Deandre','Mccall',8,2,8,0),(149,'Steve','Evans',9,4,8,0),(150,'Roberto','Potter',10,2,8,0),(151,'Richie','Wise',11,2,8,0),(152,'Wilmer','Reilly',12,4,8,0),(153,'Eldon','Jackson',13,1,8,0),(154,'Renato','Allison',14,2,8,0),(155,'Elden','Gamble',15,1,8,0),(156,'Craig','Yoder',16,2,8,0),(157,'Reynaldo','Yu',17,1,8,0),(158,'Pasquale','Merritt',18,2,8,0),(159,'Jonathan','Keller',19,1,8,0),(160,'Anibal','Cunningham',15,1,8,0),(161,'Ramon','Gardner',1,2,9,1),(162,'Otis','Macias',2,2,9,0),(163,'Morris','Shah',3,3,9,0),(164,'Juan','Giles',4,4,9,0),(165,'Benito','Schultz',5,1,9,0),(166,'Enoch','Mccall',6,3,9,0),(167,'Keneth','Pacheco',7,4,9,0),(168,'Ty','Holloway',8,1,9,0),(169,'Mohammed','Reese',9,3,9,0),(170,'Chauncey','Weaver',10,1,9,0),(171,'Roberto','Cross',11,1,9,0),(172,'Coleman','Branch',12,1,9,0),(173,'Ben','Mcpherson',13,2,9,0),(174,'Hosea','Patrick',14,2,9,0),(175,'Gary','Esparza',15,3,9,0),(176,'Reyes','George',16,1,9,0),(177,'Leroy','Guerrero',17,3,9,0),(178,'Hipolito','Zhang',18,4,9,0),(179,'Dusty','Stephenson',19,3,9,0),(180,'Lino','Sheppard',6,2,9,0),(181,'Brendon','Ali',1,1,10,1),(182,'Milton','Bridges',2,3,10,0),(183,'Booker','Ochoa',3,4,10,0),(184,'Darwin','Boone',4,4,10,0),(185,'Elmer','Whitney',5,1,10,0),(186,'Elisha','Hester',6,3,10,0),(187,'Miguel','Robbins',7,3,10,0),(188,'Edmundo','Holland',8,3,10,0),(189,'Kenneth','Hart',9,3,10,0),(190,'Kennith','Cain',10,4,10,0),(191,'Errol','Leblanc',11,2,10,0),(192,'Steve','Fernandez',12,2,10,0),(193,'Benton','Kirby',13,4,10,0),(194,'Cedrick','Perez',14,2,10,0),(195,'Dana','Moss',15,2,10,0),(196,'Lazaro','Atkinson',16,1,10,0),(197,'Gavin','Paul',17,2,10,0),(198,'Bill','Beard',18,2,10,0),(199,'Kurt','Hayes',19,4,10,0),(200,'Garret','Brooks',13,2,10,0),(201,'Heath','Harding',1,2,11,1),(202,'Kennith','Bruce',2,1,11,0),(203,'Jed','Mcgee',3,3,11,0),(204,'Ruben','Lane',4,1,11,0),(205,'Russel','Daugherty',5,4,11,0),(206,'Ramon','Crane',6,4,11,0),(207,'Wendell','Soto',7,3,11,0),(208,'Shawn','Lin',8,3,11,0),(209,'Buck','Walter',9,3,11,0),(210,'Lucas','Mcclain',10,1,11,0),(211,'Rolland','Garner',11,4,11,0),(212,'Felton','Watts',12,3,11,0),(213,'Eliseo','Becker',13,4,11,0),(214,'Erasmo','Lutz',14,4,11,0),(215,'Neville','Mercer',15,4,11,0),(216,'Delmar','Lee',16,4,11,0),(217,'Lacy','Hess',17,4,11,0),(218,'Rueben','Jacobs',18,2,11,0),(219,'Randy','Gamble',19,2,11,0),(220,'Santos','Berger',12,2,11,0);
/*!40000 ALTER TABLE `player` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-03-11  9:28:36
