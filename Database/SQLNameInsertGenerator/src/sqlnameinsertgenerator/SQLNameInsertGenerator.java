/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package sqlnameinsertgenerator;

import java.io.FileReader;
import java.util.Random;
import java.util.Scanner;
/**
 *
 * @author Bruno Severo Bataglin
 */
public class SQLNameInsertGenerator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String sqlQuery = "INSERT INTO Player (FirstName, LastName, PositionID, SkillLevelID, TeamID, Captain)\nVALUES ";
        
        try{
             Scanner myFile = new Scanner(new FileReader("ListName.txt")); //open the file
             
             String name;
             String[] splitName;
             
             if (myFile.hasNextLine()){
                 //is there anything in the file?
                                  
                 for (int i = 1; i < 12; i++) {
                     for (int j = 0; j < 20; j++) {
                         
                    name = myFile.nextLine();
                    splitName = name.split(" ");
                    int position = j < 19 ? j+1:Random(1,19);
                    int skillLevel = Random(1,4);
                    int isCaptain = j==0?1:0;
                    char comma = j == 19 && i == 11 ? ';' : ',';
                    
                    sqlQuery += String.format("('%s','%s',%d,%d,%d,%d)%s\n",
                            splitName[0],splitName[1], position, skillLevel,i, isCaptain, comma);
                     }
                    
                 }
                 System.out.println(sqlQuery);
             }
             else{
                 //nothing there
                 System.out.println("Empty file");
             }
             
        }catch (Exception e) {
            
             System.out.println("File not found!");
        }
    }
    static int Random (int min, int max)
    {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }
}
